//
// Created by brunoj on 02.12.17.
//

#include <random>
#include <algorithm>
#include <climits>
#include <sys/socket.h>
#include <functional>
#include "Genetic.h"
Graph *ga;
Genetic::Genetic(Graph *g) {
    this->graph=g;
    ga=g;
}
double getRandomDouble()
{
    return (double)rand() / (double)RAND_MAX;
}
vector<int> Genetic::generatePerm(vector<int> tmp)
{
    int iloscMiast = this->graph->getV();
    for(int i=1;i<iloscMiast;i++)
    {
        tmp.push_back(i);

    }


    random_shuffle(tmp.begin(), tmp.end());
    tmp.push_back(0);

    tmp.insert(tmp.begin(),0);

    return tmp;

}

int Genetic::getCost(vector<int> droga)
{
    int cost = 0;
    for(int i=0;i<graph->getV();i++)
    {
        int start=droga[i];
        int end = droga[i+1];
        cost=cost+graph->odleglosc[start][end];
    }
    return cost;
}
void Genetic::zmiana(vector<int> perm, int od1, int od2)
{
   int tmp = perm[od1];
   perm[od1] = perm[od2];
   perm[od2] =  tmp;
}

void Genetic::chanceOfMutate(vector<int> *perm)
{
    if(getRandomDouble()<this->mutationChace) mutate(*perm);
}
void Genetic::mutate(vector<int> dziecko)
{
    for(int i=0;i<this->changesInMutation;i++){
        zmiana(dziecko,rand()%this->graph->getV(),rand()%this->graph->getV());
    }
}
int getCost(vector<int> droga)
{
    int cost = 0;
    for(int i=0;i<ga->getV();i++)
    {
        int start=droga[i];
        int end = droga[i+1];
        cost=cost+ga->odleglosc[start][end];
    }
    return cost;
}
bool porDrogi(vector<int>i, vector<int> j){
//    int a = getCost(i);
//    int b = getCost(j);
    return getCost(i)<getCost(j);
}
//auto Genetic::compareFunc = [](vector<int> a, vector<int> b) { return getCost(a) > getCost(b); };

void Genetic::genetic()
{
    int Bestbest = INT_MAX;
    this->populacja.reserve(this->populationaSize);
    this->rodzice.reserve(this->parentPopulationSize);
    this->dzieci.reserve(this->kidsPopulationSize*2);

    for(int i=0;i<this->populationaSize;i++){
        vector<int> perm;
        perm.reserve(this->graph->getV());
        perm = this->generatePerm(perm);
        this->populacja.push_back(perm);
        cout<<getCost(populacja.back())<<"\n";
    }
    for(int i=0;i<populacja.size();i++){
        cout<<getCost(populacja[i]);
        cout<<"\n";
    }

    for(int i=0;i<iteration;i++) {
        this->rodzice.clear();
        unsigned long long permSum = 0;
        vector<int> wartPerm;
        wartPerm.resize(this->populationaSize);
        unsigned long long aktualWart = 0;
        unsigned long long koniecWart = 0;

        for (int i = 0; i < this->populationaSize; i++) {
            wartPerm[i] = getCost(populacja[i]);
            permSum = permSum + wartPerm[i];
        }

        unsigned long long helper;
        for (int i = 0; i < populationaSize; i++) {
            helper = getRandomDouble() * permSum;
            koniecWart = 0;
            aktualWart = 0;
            for (int i = 0; i < populationaSize; i++) {
                aktualWart += wartPerm[i];

                if (koniecWart <= helper && helper <= aktualWart) {
                    rodzice.push_back(populacja[i]);
                    break;
                }
                koniecWart += wartPerm[i];
            }
        }


        //krzyzowanie

        for (int i = 0; i < kidsPopulationSize; i++) {
            vector<int> mother = rodzice[rand() % parentPopulationSize];
            vector<int> father = rodzice[rand() % parentPopulationSize];

            vector<int> son;
            vector<int> daughter;
            son.resize(this->graph->getV()+1);
            daughter.resize(this->graph->getV()+1);

            for (int i = 0; i < this->graph->getV()+1; i++) {
                son[i] = father[i];
                daughter[i] = mother[i];
            }

            int miejsce1 = rand() % this->graph->getV()+1;
            int miejsce2 = rand() % this->graph->getV()+1;
            if(miejsce1==graph->getV()) miejsce1-=1;
            if(miejsce2==graph->getV()) miejsce2-=1;

            if (miejsce1 > miejsce2) {
                int tmp = miejsce2;
                miejsce2 = miejsce1;
                miejsce1 = tmp;
            }

            for (int i = miejsce1; i <= miejsce2; ++i) {
                int tmp = son[i];
                son[i] = daughter[i];
                daughter[i] = tmp;
            }
            int daughterZmiana = miejsce1;
            int sonZmiana = miejsce1;
            for (int i = miejsce1; i <= miejsce2; i++) {
                for (int j = 1; j < daughter.size(); j++) {
                    if(j==i) continue;
                    if (son[j] == son[i]){
                     for(int z=miejsce1;z<=miejsce2;z++){
                         int tmp = daughter[z];
                         if(find(son.begin(),son.end(),tmp)==son.end()){
                                son[j]=daughter[z];
                         }
                     }
                    }
                    if (daughter[j] == daughter[i]){
                        for(int z=miejsce1;z<=miejsce2;z++){
                            int tmp = son[z];
                            if(find(daughter.begin(),daughter.end(),tmp)==daughter.end()){
                                daughter[j]=son[z];
                            }
                        }
                    }
                }
            }

            chanceOfMutate(&daughter);
            chanceOfMutate(&son);
            dzieci.push_back(daughter);
            dzieci.push_back(son);
        }

        vector<vector<int>> nastepnaPopulacja;
        nastepnaPopulacja.reserve(this->populationaSize);

        sort(populacja.begin(),populacja.end(), porDrogi);
        sort(dzieci.begin(),dzieci.end(),porDrogi);

        for (int i = 0; i < populationaSize; i++) {
            if (dzieci.empty() && !populacja.empty()) {
                nastepnaPopulacja.push_back(populacja[populacja.size() - 1]);
                populacja.pop_back();
                continue;
            }
            if (!dzieci.empty() && populacja.empty()) {
                nastepnaPopulacja.push_back(dzieci[dzieci.size() - 1]);
                dzieci.pop_back();
                continue;
            }
            if (getCost(populacja.back()) > getCost(dzieci.back())) {
                cout<<"populacja "<<getCost(populacja.back())<<"\n";
                cout<<"dzieci "<<getCost(dzieci.back())<<"\n";
                nastepnaPopulacja.push_back(dzieci[dzieci.size() - 1]);
                dzieci.pop_back();
                populacja.pop_back();


            } else {
                cout<<"populacja "<<getCost(populacja.back())<<"\n";
                cout<<"dzieci "<<getCost(dzieci.back())<<"\n";
                nastepnaPopulacja.push_back(populacja[populacja.size() - 1]);
                populacja.pop_back();
                dzieci.pop_back();

            }
        }
        while (!populacja.empty()) {
            populacja.pop_back();
        }
        while (!dzieci.empty()) {
            dzieci.pop_back();
        }
        populacja = nastepnaPopulacja;
        int dlugosc = getCost(populacja.back());
        int best = dlugosc;
        cout<<"front: "<<dlugosc<<"\n";
        int tmpBest = best;
        if (tmpBest < Bestbest) {
            Bestbest = tmpBest;
        }
    }
        while(!populacja.empty()){
            populacja.pop_back();
        }
        rodzice.clear();
        populacja.clear();
        cout<<"rozwiazanie "<<Bestbest;


}
