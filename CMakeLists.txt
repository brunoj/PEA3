cmake_minimum_required(VERSION 3.8)
project(PEA3)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Graph.cpp Graph.h Genetic.cpp Genetic.h)
add_executable(PEA3 ${SOURCE_FILES})