//
// Created by brunoj on 02.12.17.
//

#ifndef PEA3_GENETIC_H
#define PEA3_GENETIC_H


#include "Graph.h"

class Genetic {
private:
    Graph *graph;
    vector<vector<int>> populacja;
    vector<vector<int>> rodzice;
    vector<vector<int>> dzieci;

public:
    Genetic(Graph *g);
    void sortT(vector<vector<int>> populacja);
    int mutationChace=5;
    int changesInMutation=20;
    int populationaSize=150;
    int iteration=50;
    int parentPopulationSize=50;
    int kidsPopulationSize=50;
    vector<int> generatePerm(vector<int> tmp);
    int getCost(vector<int> droga);
    void genetic();
    void mutate(vector<int> dziecko);
    void chanceOfMutate(vector<int> *perm);
    void zmiana(vector<int> perm,int od1,int od2);

};


#endif //PEA3_GENETIC_H
